<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//TODO
Route::get('todo','Organiser\ToDoController@Todos');
Route::get('todo/{id}','Organiser\ToDoController@getTodo');
Route::get('todo/delete/{id}','Organiser\ToDoController@delete');
Route::post('todo/update','Organiser\ToDoController@update');
Route::post('todo/create','Organiser\ToDoController@create');
Route::get('todo/{id}/{status}','Organiser\ToDoController@updateStatus');
//ACTIVITY-#[Note]
Route::get('activity','Organiser\NoteController@Activity');
Route::get('activity/{id}','Organiser\NoteController@getActivity');
Route::post('activity/create','Organiser\NoteController@createActivity');
Route::get('activity/delete/{id}','Organiser\NoteController@deleteActivity');
//Note
Route::get('activity/{id}/note', 'Organiser\NoteController@Note');
Route::post('note/create', 'Organiser\NoteController@createNote');
Route::get('note/delete/{id}', 'Organiser\NoteController@deleteNote');
Route::get('note/{id}','Organiser\NoteController@getNote');
Route::post('note/update','Organiser\NoteController@updateNote');