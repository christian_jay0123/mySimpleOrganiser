//post API
const postNote = async(data,url,myfunction) =>
{
    setting = {
        method:"POST",
          headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        body:JSON.stringify(data),
    }
    
    let response = await fetch(url,setting);
    let res = await response.json();
    myfunction(res);
}

//get API
const getNote = async(url,myfunction) =>
{
    let response = await fetch(url);
    let res = await response.json();
    myfunction(res)
}
//
    const notelist = (data) =>
    {
        $('#content').empty();
        data.forEach((data,i)=>{
            $('#content').append(`<div class="card text-center" id="card_${data.id}">
            <div class="card-header"><p>${data.updated_at}</p>
            <div class="card-body">
              <h4 class="card-title"><label">Title:${data.title}</label></h4>
              <h5>Content:${data.body}</h5>
              <a href="#" class="btn btn-danger deleteNote" value="${data.id}">Delete</a> <a href="#" class="btn btn-primary updateNote" value="${data.id}"  data-toggle="modal" data-target="#updateNotesModal">Update</a> 
            </div>
          </div><br>`);
            });
    }
//
    const specificActivity = (data) =>
    {
        $('#page').append(`${data.activity}<a href="#" data-toggle="modal" data-target="#addNotesModal"><span class="glyphicon glyphicon-plus"></span></a>`);
    }

    const notes = (data) =>
    {
        notelist(data);
    }
//
$(document).on('click','.viewActivity',function(e){
    let id = $(this).attr('value');
    let url = `api/activity/${id}`;
    $('.createNote').val(id);
    e.preventDefault();
    $('#content').empty();
    $('#page').empty();
    $('#detail').empty();
    let url1 = `api/activity/${id}/note`;
    getNote(url1,notes);
    getNote(url,specificActivity);
});
//
    const createNote = (data) =>
    {
        notelist(data);
    }

//
$(document).on('click','.createNote',function(e){
    e.preventDefault();
    let data = {
        title:$('#inputboxcreatenote').val(),
        body:$('#textareacreatenote').val(),
        id:$('.createNote').val()
    }
    let url = `api/note/create`;
    postNote(data,url,createNote);
});
//
    const deleteNote = (data) =>
    {
        notelist(data);
    }
//
$(document).on('click','.deleteNote',function(){
    let id = $(this).attr('value');
    let url = `api/note/delete/${id}`;
    getNote(url,deleteNote);
});
//
    const dataNote = (data) =>
    {
        $('#inputboxupdatenote').val(data.title);
        $('#textareaupdatenote').val(data.body);
        $('.saveNote').val(data.id);
    }
//
$(document).on('click','.updateNote',function(){
    let id = $(this).attr('value');
    let url = `api/note/${id}`;
    getNote(url,dataNote)
});
//
    const saveNote = (data) =>
    {
        notelist(data);
    }
//
$(document).on('click','.saveNote',function(){
    let data = {
        title:$('#inputboxupdatenote').val(),
        body:$('#textareaupdatenote').val(),
        id:$('.saveNote').val()
    }
    let url = `api/note/update`;
    postNote(data,url,saveNote);
});