//post API
const postActivity = async(data,url,myfunction) =>
{
    setting = {
        method:"POST",
          headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        body:JSON.stringify(data),
    }
    
    let response = await fetch(url,setting);
    let res = await response.json();
    myfunction(res);
}
//get API
 const getActivity = async(url,myfunction) =>
 {
    let response = await fetch(url);
    let res = await response.json();
    myfunction(res);
 } 
//
        const Activitylist = (data) =>
        {
            $('#content').empty();
        data.forEach((activity,i)=>{
            $('#content').append(`<div class="card text-center" >
            <div class="card-header"><p>${activity.created_at}</p>
            <div class="card-body">
              <h4 class="card-title">Activity:<label>${activity.activity}</label></h4>
              <a href="#" class="btn btn-danger deleteActivity" value="${activity.id}">Delete</a> <a href="#" class="btn btn-primary viewActivity" value="${activity.id}">View Notes</a>
            </div>
          </div><br>`);
        });
        }
//
    const Activity = (data) =>
    {
        Activitylist(data);
    }
//
$('#activity').click(function(e){
    e.preventDefault();
    $('#page').empty();
    $('#detail').empty();
    $('#page').append(`Activity <a href="#" data-toggle="modal" data-target="#addActivityModal"><span class="glyphicon glyphicon-plus"></span></a>`);
    $('#detail').append(`This is my activity list`);
    let url = `api/activity`
    getActivity(url, Activity);
});
//
    const createActivity = (data) =>
    {
        Activitylist(data);
    }
//
$('.createActivity').click(function(){
    let data = {
        activity:$('#todoinputboxcreateactivity').val()
    }
    let url = `api/activity/create`;
    postActivity(data,url,createActivity);
});
//
    const deleteUpdate = (data) =>
    {
        Activitylist(data);
    }
//
$(document).on('click','.deleteActivity',function(e){
e.preventDefault();
let id = $(this).attr('value');
let url = `api/activity/delete/${id}`;
getActivity(url,deleteUpdate);
});

