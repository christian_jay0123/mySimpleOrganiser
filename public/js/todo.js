//post API
const post = async(data,url,myfunction) =>
{
    setting = {
        method:"POST",
          headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        body:JSON.stringify(data),
    }
    
    let response = await fetch(url,setting);
    let res = await response.json();
    myfunction(res);
}

//get API
const get = async(url,myfunction) =>
{
    let response = await fetch(url);
    let res = await response.json();
    myfunction(res)
}

    //functions
    const strike = (data) =>
    {
       let todo = data.todo;
        if (data.status == 0){
            let strikes = {
                todo:todo,
                status:''
            }
            return strikes;
        }else if(data.status == 1){
            let strikes = {
                todo:todo.strike(),
                status:'checked'
            }
            return strikes;
        }
    }
    const todolist = (data) =>
    {
    $('#content').empty();
    data.forEach((data,i)=>{
      let strikeValue = {
          todo:data.todo,
          status:data.status,
          id:data.id,
      };
    $('#content').append(`<div class="card text-center" id="card_${data.id}">
    <div class="card-header"><p id="time_${data.id}">${data.updated_at}</p>
    <div class="card-body">
      <h4 class="card-title">
      <input type="checkbox" id="chc_${data.id}" class="checks" value="${data.id}" ${strike(strikeValue).status}><label id="todo_${data.id}">${strike(strikeValue).todo}</label></h4>
      <a href="#" class="btn btn-danger delete" id="del_${data.id}">Delete</a> <a href="#" class="btn btn-primary update"  id="up_${data.id}" data-toggle="modal" data-target="#updateModal">Update</a> 
    </div>
  </div><br>`);
    });
    }

//buttons
$('#todo').click(function(e){
    e.preventDefault();
    $('#page').empty();
    $('#detail').empty();
    $('#page').append(`Todo List <a href="#" data-toggle="modal" data-target="#addModal"><span class="glyphicon glyphicon-plus"></span></a>`);
    $('#detail').append(`This is my Todo list`);
    let url = `api/todo`;
    get(url,todolist);
});
//
    const deleteResponse = () =>
    {
        //reserve
    }
//
$(document).on('click','.delete',function(e){
    e.preventDefault();
    let id = $(this).attr('id').replace('del_','');
    $(`#card_${id}`).remove();
    let url = `api/todo/delete/${id}`;
    get(url,deleteResponse);
});
//    
    const findData = (data) =>
    {
        $('#todoinputbox').val(data.todo);
        $('.saveChange').val(data.id);
    }
//
$(document).on('click','.update',function(e){
    let id = $(this).attr('id').replace('up_','');
    let url = `api/todo/${id}`;
    get(url,findData);
});
//
    const update = (data) =>
    {
        let strikeValue = {
            todo:data.todo,
            status:data.status,
            id:data.id,
        };
            $(`#todo_${data.id}`).empty();
            $(`#todo_${data.id}`).append(strike(strikeValue).todo);
            $(`#time_${data.id}`).empty();
            $(`#time_${data.id}`).append(data.updated_at)
    }
//
$(document).on('click','.saveChange',function(e){
    e.preventDefault();
    let value = $('#todoinputbox').val();
    let id = $(this).val();
    let url = `api/todo/update`;
    let data = {
        'todo':value,
        'id':id
    };
    post(data,url,update)
});
//
   const UpdateStatus = (data) =>
    {
        let strikeValue = {
            todo:data.todo,
            status:data.status,
            id:data.id,
        };
        $(`#time_${data.id}`).empty();
        $(`#time_${data.id}`).append(data.updated_at);
        $(`#todo_${data.id}`).empty();
        $(`#todo_${data.id}`).append(`${strike(strikeValue).todo}`);
    }
//
$(document).on('click','.checks',function(){
    let id = $(this).val();
    ($(this).is(':checked'))?status = 1 :status = 0;
    let url = `api/todo/${id}/${status}`;
    get(url,UpdateStatus);
});
//
    const create = (data) =>
    {
        todolist(data);
    }
//
$(document).on('click','.create',function(){
    let data = {
        'todo':$('#todoinputboxcreate').val()
    }
    let url = `api/todo/create`;
    post(data,url,create);
});