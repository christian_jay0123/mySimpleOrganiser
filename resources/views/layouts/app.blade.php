<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <span class="navbar-brand">Organiser</span>
    </div>
    <ul class="nav navbar-nav">
      <li><a id='home' href="">Home</a></li>
      <li><a id='activity' href="">Activity</a></li>
      <li><a id='todo' href="">Todo List</a></li>
    </ul>
  </div>
</nav>
    @yield('content')
{{--  --}}
    <div class="modal fade" id="updateModal" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content col-sm-7">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><center>Todo</center></h4>
            </div>
            <div class="modal-body">
              <center><input id='todoinputbox' class="form-control" type="text"></center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default saveChange" data-dismiss="modal">Save Change</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
{{--  --}}
      <div class="modal fade" id="addModal" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content col-sm-7">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><center>Todo</center></h4>
            </div>
            <div class="modal-body">
              <center><input class="form-control" placeholder="What do you want to do?" id='todoinputboxcreate' type="text"></center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default create" data-dismiss="modal">Save</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          
        </div>
      </div>
{{--  --}}
      <div class="modal fade" id="addActivityModal" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content col-sm-7">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><center>Activity</center></h4>
            </div>
            <div class="modal-body">
              <center><input type="text" class="form-control" placeholder="What's on your mind?" id='todoinputboxcreateactivity' ></center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default createActivity" data-dismiss="modal">Save</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
          
        </div>
      </div>
{{--  --}}
    <div class="modal fade" id="addNotesModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content col-sm-7">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><center>Note</center></h4>
          </div>
          <div class="modal-body">
            <input type="text" class="form-control" placeholder="Title" id='inputboxcreatenote' ><br>
            <textarea class="form-control" placeholder="Content" id='textareacreatenote' ></textarea>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default createNote" data-dismiss="modal">Add note</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        
      </div>
    </div>
{{--  --}}
    <div class="modal fade" id="updateNotesModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content col-sm-7">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><center>Note</center></h4>
          </div>
          <div class="modal-body">
            <input type="text" class="form-control" placeholder="Title" id='inputboxupdatenote' ><br>
            <textarea class="form-control" placeholder="Content" id='textareaupdatenote' ></textarea>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default saveNote" data-dismiss="modal">Save Change</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        
      </div>
    </div>
</body>

    @yield('js')
</html>
