@extends('layouts.app')
@section('content')
<div class="jumbotron">
    <center><h1 class="display-4" id="page">My Organiser!</h1></center>
    <center><p class="lead" id="detail">This is my organiser please remind that this can organise my time.</p></center>
    <hr class="my-4">
    <p id="content"> </p>
</div>
@stop
@section('js')
<script src="{{asset('js/activity.js')}}"></script>
<script src="{{asset('js/todo.js')}}"></script>
<script src="{{asset('js/home.js')}}"></script>
<script src="{{asset('js/note.js')}}"></script>
@stop