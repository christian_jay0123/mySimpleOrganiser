<?php

namespace App\Http\Controllers\Organiser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Organiser\Todo;
class ToDoController extends Controller
{
    
    public function Todos()
    {
        return response()->json(Todo::select()->where('status',0)->Orwhere('status',1)->OrderBy('updated_at','DESC')->get());
    }

    public function getTodo($id)
    {
        return Todo::find($id);
    }

    public function delete($id)
    {
        $todo = Todo::find($id);
        $todo->status = 2;
        $todo->save();
        return response()->json('success');
    }

    public function validateDataUpdate($request)
    {
        $validator = \Validator::make($request->all(), ['todo' => 'required|max:200']);
        return ($validator->fails())? ['failed'=>'Missing Text'] : $this->successDataUpdate($request) ;
    }

    public function successDataUpdate($request)
    {
        $todo = Todo::find($request->input('id'));
        $todo->todo = $request->input('todo');
        $todo->save();
        return $todo;
    }
    
    public function update(Request $request)
    {
        return response()->json($this->validateDataUpdate($request));
    }
    
    public function updateStatus($id, $status)
    {
        $todo = Todo::find($id);
        $todo->status = $status;
        $todo->save();
        return response()->json($todo);
    }

    public function validateDataCreate($request)
    {
        $validator = \Validator::make($request->all(), ['todo' => 'required|max:200']);
        return ($validator->fails())? ['failed'=>'Missing Text'] : $this->successDataCreate($request) ;
    }

    public function successDataCreate($request)
    {
        $todo = new Todo;
        $todo->todo = $request->input('todo');
        $todo->status = 0;
        $todo->save();
        return Todo::select()->where('status',0)->Orwhere('status',1)->OrderBy('updated_at','DESC')->get();
    }

    public function create(Request $request)
    {
        return response()->json($this->validateDataCreate($request));
    }
}
