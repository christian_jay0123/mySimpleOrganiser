<?php

namespace App\Http\Controllers\Organiser;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Organiser\Activity;
use App\Organiser\Note;
class NoteController extends Controller
{
    //activity
    public function Activity()
    {
        $activity = new Activity;
        return response()->json($activity->updatedActivity());
    }

    public function getActivity($id)
    {
        $activity = Activity::find($id);
        return response()->json($activity);
    }

    public function validateDataCreateActivity($request)
    {
        $validator = \Validator::make($request->all(), ['activity' => 'required|max:200']);
        return ($validator->fails())? ['failed'=>'Missing Text'] : $this->successDataCreateActivity($request) ;
    }

    public function successDataCreateActivity($request)
    {
        $activity = new Activity;
        $activity->activity = $request->input('activity');
        $activity->status = 0;
        $activity->save();
        return $activity->updatedActivity();
    }

    public function createActivity(Request $request)
    {
        return response()->json($this->validateDataCreateActivity($request));
    }

    public function deleteActivity($id)
    {
        $activity = Activity::find($id);
        $activity->status = 2;
        $activity->save();
        return response()->json($activity->updatedActivity());
    }
    //Note
    public function Note($id)
    {
        return response()->json(Activity::find($id)->notes);
    }

    public function validateDataCreateNote($request)
    {
        $validator = \Validator::make($request->all(), ['title' => 'required|max:200']);
        return ($validator->fails())? ['failed'=>'Missing Text'] : $this->successDataCreateNote($request) ;
    }

    public function successDataCreateNote($request)
    {
        $id = $request->input('id');
        $note = new Note;
        $note->title = $request->input('title');
        $note->body = $request->input('body');
        $note->status = 0;
        $note->activity_id = $id;
        $note->save();
        return Activity::find($id)->notes;
    }

    public function createNote(Request $request)
    {
        return response()->json($this->validateDataCreateNote($request));
    }   

    public function deleteNote($id)
    {
        $note = Note::find($id);
        $note->status = 2;
        $note->save();
        return response()->json(Activity::find($note->activity_id)->notes);
    }

    public function getNote($id)
    {
        return response()->json(Note::find($id));
    }

    public function validateDataUpdateNote($request)
    {
        $validator = \Validator::make($request->all(), ['title' => 'required|max:200']);
        return ($validator->fails())? ['failed'=>'Missing Text'] : $this->successDataUpdateNote($request) ;
    }

    public function successDataUpdateNote($request)
    {
        $note = Note::find($request->input('id'));
        $note->title = $request->input('title');
        $note->body = $request->input('body');
        $note->save();
        return Activity::find($note->activity_id)->notes;
    }

    public function updateNote(Request $request)
    {
        return response()->json($this->validateDataUpdateNote($request));
    }
}
