<?php

namespace App\organiser;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{

    public function notes()
    {
        return $this->hasMany(Note::class)->where('status',0)->orderBy('updated_at','desc');
    }

    public function updatedActivity()
    {
       return Activity::select()->where('status',0)->OrderBy('updated_at','DESC')->get();
    }
}
